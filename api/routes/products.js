const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Product = new require('../models/product');

router.get('/', (req, res) => {
    Product.find((err,products) => {
        if (err) res.status(404).json(err);
        products.forEach(product => delete product._doc.__v);
        res.status(200).json(products)
    });
});

router.post('/', (req, res) => {
        new Product({
            _id: new mongoose.Types.ObjectId(),
            name: req.body.name,
            price: req.body.price,
        }).save((err, product) => {
            if (err) res.status(500).json({ error: err});
            delete product._doc.__v;
            res.status(200).json(product);
        });
});

router.get('/:productId', (req, res) => {
    Product.findById(req.params.productId, (err, product)=>{
        if (err) res.status(500).json({ error: err});
        delete product._doc.__v;
        res.status(200).json(product);
    })
});

router.patch('/:productId', (req, res) => {
    Product.findByIdAndUpdate(req.params.productId, req.body, {new: true}, (err, product)=>{
        if (err) res.status(500).json(err);
        delete product._doc.__v;
        res.status(200).json(product);
    })
});

router.delete('/:productId', (req, res) => {
    Product.remove((err, product)=>{
        if (err) res.status(500).json(err);
        delete product._doc.__v;
        res.status(200).json(product);
    });
});

module.exports = router;