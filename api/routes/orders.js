const express = require('express');
const router = express.Router();

router.get('/', (req, res)=>{
    res.status(200).json({
        "message": "Orders were fetched",
        url: req.urlencoded
    })
});

router.post('/', (req, res)=>{
    res.status(201).json({
        "message": "Order was created",
        order: req.body,
    })
});

router.get('/:orderId', (req, res)=>{
        res.status(200).json({
            "message": "Order details",
            orderId: req.params.orderId
        });
});

router.patch('/:orderId', (req, res) => {
    res.status(200).json({
        message: "Updated order",
    })
});

router.delete('/:orderId', (req, res) => {
    res.status(200).json({
        message: "Deleted order"
    })
})

module.exports = router;