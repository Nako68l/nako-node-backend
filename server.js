const http = require('http');
const fs = require('fs');
const app = require('./app');

const port = process.env.PORT || 4242;
//
// {
//     key: fs.readFileSync('key.pem'),
//         cert: fs.readFileSync('cert.pem')
// },
http.createServer(app).listen(port, (error) => {
    if (error) {
        console.error(error);
        return process.exit(1)
    } else {
        console.log('Listening on port: ' + port + '.')
    }
});