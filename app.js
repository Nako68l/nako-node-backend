const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const app = express();

const productRouts = require('./api/routes/products');
const ordersRouts = require('./api/routes/orders');
// const landing = require('./api/routes/landing');

mongoose.connect('mongodb://nako:'+ process.env.MONGO_ATLAS_PW + '@nako-shard-00-00-xdjn9.mongodb.net:27017,nako-shard-00-01-xdjn9.mongodb.net:27017,nako-shard-00-02-xdjn9.mongodb.net:27017/test?ssl=true&replicaSet=nako-shard-0&authSource=admin').catch(console.log);

// app.use('/', landing);
app.use(morgan('dev'));
app.use(bodyParser.json());


app.use((req, res, next)=>{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if (req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});
app.use('/products', productRouts);
app.use('/orders', ordersRouts);

app.use((req, res, next) => {
    let error = new Error("Not found");
    error.status = 404;
    next(error);
});

app.use((err, req, res, next)=>{
    res.status(err.status || 500).send(`<h1>${err.status}\t${err.message}</h1>`);
});
module.exports = app;